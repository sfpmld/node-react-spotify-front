import Cookies from 'js-cookie';

// to set token in cookie
const set = (key, value, expires=undefined) => {
  // window need to be available to set cookie
  if(typeof window !== 'undefined') {
    Cookies.set(key, JSON.stringify(value), {
      // one day duration
      expires: expires || 1
    })
  }
}

const get = (key) => {
  // window need to be available to get cookie
  if(typeof window !== 'undefined') {
    const cache = Cookies.get(key)
    if (cache) {
      return JSON.parse(cache);
    }
  }
}

const remove = (key) => {
  // window need to be available to remove cookie
  if(typeof window !== 'undefined') {
    Cookies.remove(key);
  }
}

export const cookies = Object.freeze({
  set,
  get,
  remove
});
