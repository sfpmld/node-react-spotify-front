import axios from 'axios';

export const axiosFetcher = function (
  url,
  method,
  settings
) {
  let res;
  switch (method) {
    case 'get':
      res = axios.get(url, settings);
      break;
    case 'post':
      res = axios.post(url, settings);
      break;
    default:
      throw new Error('bad method for axios fetcher');
  }
  return res;
};
