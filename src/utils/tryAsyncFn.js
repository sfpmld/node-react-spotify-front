export const tryAsyncFn = async (fn) => {
  try {
    return await fn();
  } catch (err) {
    throw err;
  }
};
