// to save user information
const set = (key, value) => {
  if(typeof window !== 'undefined') {
    window.localStorage.setItem(key, JSON.stringify(value));
  }
}

const get = (key) => {
  const cache = window.localStorage.getItem(key);
  if(typeof window !== 'undefined' && cache !== "undefined") {
    return JSON.parse(cache);
  }
}

const remove = (key, value) => {
  if(typeof window !== 'undefined') {
    return window.localStorage.removeItem(key, value);
  }
}

export const localStorage = Object.freeze({
  set,
  get,
  remove
});
