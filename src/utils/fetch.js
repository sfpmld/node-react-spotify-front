import { tryAsyncFn } from '../utils';

// fetcher factory
export const makeFetch = (fetcher, url) => {
  return class fetch {
    async get(path, settings) {
      return tryAsyncFn(() => fetcher(`${url}${path}`, 'get', settings));
    }
    async post(
      path,
      settings
    ) {
      return tryAsyncFn(() => fetcher(`${url}${path}`, 'post', settings));
    };
  };
};
