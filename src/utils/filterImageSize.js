import { IMG_WIDTH, IMG_HEIGHT } from '../config';
import NoImage from '../components/images/no_image_small.jpg'

export const filterImageSize =  images => {
  const image = images.filter(
    image => {
      return image.height === IMG_HEIGHT && image.width === IMG_WIDTH
    }
  );
  return image.length > 0 ? image[0].url : NoImage;
};
