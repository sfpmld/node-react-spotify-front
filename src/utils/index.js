import { makeFetch } from './fetch';
import { axiosFetcher } from './axiosFetcher';
import { API_URL } from '../config'

export * from './cookie';
export * from './localStorage';
export * from './filterImageSize';
export * from './tryAsyncFn';
export * from './isObjEmpty';
export * from './time-utils';

export const Fetcher = makeFetch(axiosFetcher, API_URL);
