export const artistsData = {
  search: {
    "href": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=0&limit=2",
    "artists": [
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/0kbYTNQb4Pb1rPbbaF0pT4"
        },
        "followers": {
          "href": null,
          "total": 1818373
        },
        "genres": [
          "bebop",
          "contemporary post-bop",
          "cool jazz",
          "hard bop",
          "jazz",
          "jazz fusion",
          "jazz trumpet"
        ],
        "href": "https://api.spotify.com/v1/artists/0kbYTNQb4Pb1rPbbaF0pT4",
        "id": "0kbYTNQb4Pb1rPbbaF0pT4",
        "images": [
          {
            "height": 1000,
            "url": "https://i.scdn.co/image/423e826b3c1b23930a255d7cbc2daf733f795507",
            "width": 1000
          },
          {
            "height": 640,
            "url": "https://i.scdn.co/image/a318c54208af38364d131a54ced2416423696018",
            "width": 640
          },
          {
            "height": 200,
            "url": "https://i.scdn.co/image/8496e6ea230dd47311d85dcf860015792f5ada42",
            "width": 200
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/b1af952a7fb8ac2c4467868d61b5752fc1a01cf0",
            "width": 64
          }
        ],
        "name": "Miles Davis",
        "popularity": 68,
        "type": "artist",
        "uri": "spotify:artist:0kbYTNQb4Pb1rPbbaF0pT4"
      },
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/71Ur25Abq58vksqJINpGdx"
        },
        "followers": {
          "href": null,
          "total": 193055
        },
        "genres": [
          "bebop",
          "contemporary post-bop",
          "cool jazz",
          "hard bop",
          "jazz",
          "jazz fusion"
        ],
        "href": "https://api.spotify.com/v1/artists/71Ur25Abq58vksqJINpGdx",
        "id": "71Ur25Abq58vksqJINpGdx",
        "images": [
          {
            "height": 723,
            "url": "https://i.scdn.co/image/7e2b6815b5973d47cccbad1d834089231d9b737e",
            "width": 999
          },
          {
            "height": 463,
            "url": "https://i.scdn.co/image/399ee45e619648be6ce992228b79354f31b69999",
            "width": 640
          },
          {
            "height": 145,
            "url": "https://i.scdn.co/image/9c69550b77ce6f3d040f32122bff0325e0dddd3c",
            "width": 200
          },
          {
            "height": 46,
            "url": "https://i.scdn.co/image/5b89bd24901e2d12347296e753027b70e0bfeacd",
            "width": 64
          }
        ],
        "name": "Miles Davis Quintet",
        "popularity": 53,
        "type": "artist",
        "uri": "spotify:artist:71Ur25Abq58vksqJINpGdx"
      }
    ],
    "limit": 2,
    "next": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=2&limit=2",
    "offset": 0,
    "previous": null,
    "total": 70
  }
};
