export const moreArtistsData = {
 "searchMore": {
      "artists": [
        {
          "id": "1i4ed4I1y7YerI0fQV4lVc",
          "name": "Miles Davis Sextet",
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273fc610e983077edf3d076bcc4",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02fc610e983077edf3d076bcc4",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851fc610e983077edf3d076bcc4",
              "width": 64,
              "height": 64
            }
          ],
          "genres": [
            "bebop",
            "cool jazz",
            "hard bop",
            "jazz"
          ],
          "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
          "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
        },
        {
          "id": "4sQVPSDmfqIxG9W8o2EROX",
          "name": "Miles Davis Quartet",
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27386b3a9589f9ccfc3e6167fe0",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0286b3a9589f9ccfc3e6167fe0",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485186b3a9589f9ccfc3e6167fe0",
              "width": 64,
              "height": 64
            }
          ],
          "genres": [
            "bebop",
            "cool jazz",
            "hard bop",
            "jazz",
            "jazz quartet"
          ],
          "href": "https://api.spotify.com/v1/artists/4sQVPSDmfqIxG9W8o2EROX",
          "uri": "spotify:artist:4sQVPSDmfqIxG9W8o2EROX"
        }
      ],
      "limit": 2,
      "offset": 2,
      "total": 72,
      "previous": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=0&limit=2",
      "next": "https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=4&limit=2"
    }
};
