export const albumByIdData = {
 "getAlbumById": {
      "id": "0P3DTd8ug4xnh6VCgjSjuF",
      "name": "The Greatest Jazz And Blues Music of Alltime, Vol. 22",
      "artists": [
        {
          "id": "1i4ed4I1y7YerI0fQV4lVc",
          "name": "Miles Davis Sextet",
          "images": null,
          "genres": null,
          "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
          "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
        }
      ],
      "images": [
        {
          "url": "https://i.scdn.co/image/ab67616d0000b2739b5c0dc33ae162d1b998716c",
          "width": 640,
          "height": 640
        },
        {
          "url": "https://i.scdn.co/image/ab67616d00001e029b5c0dc33ae162d1b998716c",
          "width": 300,
          "height": 300
        },
        {
          "url": "https://i.scdn.co/image/ab67616d000048519b5c0dc33ae162d1b998716c",
          "width": 64,
          "height": 64
        }
      ],
      "total_tracks": 6,
      "type": "album",
      "release_date": "2019-11-23",
      "href": "https://api.spotify.com/v1/albums/0P3DTd8ug4xnh6VCgjSjuF",
      "uri": "spotify:album:0P3DTd8ug4xnh6VCgjSjuF"
    }
};
