export const tracksByAlbumData = {
   "getTracksByAlbum": {
      "tracks": [
        {
          "id": "5oqCq9c9vjDNt0ZFvsvYEH",
          "name": "Straight, No Chaser",
          "track_number": 1,
          "duration_ms": 637626,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/5oqCq9c9vjDNt0ZFvsvYEH",
          "uri": "spotify:track:5oqCq9c9vjDNt0ZFvsvYEH"
        },
        {
          "id": "2qpBluxJeY7gtQn9wwiSvm",
          "name": "Two Bass Hit",
          "track_number": 2,
          "duration_ms": 313360,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/2qpBluxJeY7gtQn9wwiSvm",
          "uri": "spotify:track:2qpBluxJeY7gtQn9wwiSvm"
        },
        {
          "id": "7qno8LWVR9huLIGATyOKfv",
          "name": "Milestone",
          "track_number": 3,
          "duration_ms": 344866,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/7qno8LWVR9huLIGATyOKfv",
          "uri": "spotify:track:7qno8LWVR9huLIGATyOKfv"
        },
        {
          "id": "7kHRLuxbdBwoo8lkqwRO7I",
          "name": "Sid's Ahead",
          "track_number": 4,
          "duration_ms": 782133,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/7kHRLuxbdBwoo8lkqwRO7I",
          "uri": "spotify:track:7kHRLuxbdBwoo8lkqwRO7I"
        },
        {
          "id": "06q2KJUFAfy0zyNOSxCwYK",
          "name": "Milestone",
          "track_number": 5,
          "duration_ms": 432933,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/06q2KJUFAfy0zyNOSxCwYK",
          "uri": "spotify:track:06q2KJUFAfy0zyNOSxCwYK"
        },
        {
          "id": "4XwH8Etwa4RhfUOsOaevpI",
          "name": "Dr. Jackel",
          "track_number": 6,
          "duration_ms": 349133,
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "href": "https://api.spotify.com/v1/tracks/4XwH8Etwa4RhfUOsOaevpI",
          "uri": "spotify:track:4XwH8Etwa4RhfUOsOaevpI"
        }
      ]
    }
};
