export const artistByIdData = {
  "getArtistById": {
        "id": "1i4ed4I1y7YerI0fQV4lVc",
        "name": "Miles Davis Sextet",
        "images": [
          {
            "url": "https://i.scdn.co/image/ab67616d0000b273fc610e983077edf3d076bcc4",
            "width": 640,
            "height": 640
          },
          {
            "url": "https://i.scdn.co/image/ab67616d00001e02fc610e983077edf3d076bcc4",
            "width": 300,
            "height": 300
          },
          {
            "url": "https://i.scdn.co/image/ab67616d00004851fc610e983077edf3d076bcc4",
            "width": 64,
            "height": 64
          }
        ],
        "genres": [
          "bebop",
          "cool jazz",
          "hard bop",
          "jazz"
        ],
        "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
        "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
      }
};
