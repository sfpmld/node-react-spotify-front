export const albumsByArtistData = {
  "getAlbumsByArtistId": {
      "albums": [
        {
          "id": "0P3DTd8ug4xnh6VCgjSjuF",
          "name": "The Greatest Jazz & Blues Music of Alltime, Vol. 22",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b2739b5c0dc33ae162d1b998716c",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e029b5c0dc33ae162d1b998716c",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d000048519b5c0dc33ae162d1b998716c",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 6,
          "type": "album",
          "release_date": "2019-11-23",
          "href": "https://api.spotify.com/v1/albums/0P3DTd8ug4xnh6VCgjSjuF",
          "uri": "spotify:album:0P3DTd8ug4xnh6VCgjSjuF"
        },
        {
          "id": "0skWYpRoYmjbFW19Zcza4g",
          "name": "Billy Boy",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273afa3d343cb9b1e7e6b0dbc20",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02afa3d343cb9b1e7e6b0dbc20",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851afa3d343cb9b1e7e6b0dbc20",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 6,
          "type": "album",
          "release_date": "2019-01-18",
          "href": "https://api.spotify.com/v1/albums/0skWYpRoYmjbFW19Zcza4g",
          "uri": "spotify:album:0skWYpRoYmjbFW19Zcza4g"
        },
        {
          "id": "0161RGO18c2rGt3QkJOUT6",
          "name": "Milestones",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273fc610e983077edf3d076bcc4",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02fc610e983077edf3d076bcc4",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851fc610e983077edf3d076bcc4",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 24,
          "type": "album",
          "release_date": "2016-09-17",
          "href": "https://api.spotify.com/v1/albums/0161RGO18c2rGt3QkJOUT6",
          "uri": "spotify:album:0161RGO18c2rGt3QkJOUT6"
        },
        {
          "id": "0SIV8sVoJYhXqjLOU00VlS",
          "name": "April in Paris",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273da7972d67b2ce365419c3134",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02da7972d67b2ce365419c3134",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851da7972d67b2ce365419c3134",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 48,
          "type": "album",
          "release_date": "2013-09-20",
          "href": "https://api.spotify.com/v1/albums/0SIV8sVoJYhXqjLOU00VlS",
          "uri": "spotify:album:0SIV8sVoJYhXqjLOU00VlS"
        },
        {
          "id": "6WkUNo2O2BgYoo7v82eSwn",
          "name": "Someday My Prince Will Come (Original Album - Remastered)",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27330363c7b9bdf97ebb3b0ff40",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0230363c7b9bdf97ebb3b0ff40",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485130363c7b9bdf97ebb3b0ff40",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 6,
          "type": "album",
          "release_date": "2011-12-07",
          "href": "https://api.spotify.com/v1/albums/6WkUNo2O2BgYoo7v82eSwn",
          "uri": "spotify:album:6WkUNo2O2BgYoo7v82eSwn"
        },
        {
          "id": "1Bc8yXUDVUAhGpxtsdhc6Q",
          "name": "Live At The Hollywood Bowl 1981",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27320a4c617188b17a177fcaa7f",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0220a4c617188b17a177fcaa7f",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485120a4c617188b17a177fcaa7f",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 7,
          "type": "album",
          "release_date": "2010-05-14",
          "href": "https://api.spotify.com/v1/albums/1Bc8yXUDVUAhGpxtsdhc6Q",
          "uri": "spotify:album:1Bc8yXUDVUAhGpxtsdhc6Q"
        },
        {
          "id": "47h5WihkGtCVj3f35sjPzs",
          "name": "Fillmore West, April 1970",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27385bce03bb031af996fc6404e",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0285bce03bb031af996fc6404e",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485185bce03bb031af996fc6404e",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 8,
          "type": "album",
          "release_date": "1970",
          "href": "https://api.spotify.com/v1/albums/47h5WihkGtCVj3f35sjPzs",
          "uri": "spotify:album:47h5WihkGtCVj3f35sjPzs"
        },
        {
          "id": "4bBbkwePNn99UrJrSNIs1L",
          "name": "Miles and Monk at Newport (Live)",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            },
            {
              "id": "2ZUAe0H2nhsuuCOykSVsJ2",
              "name": "Thelonious Monk Quartet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/2ZUAe0H2nhsuuCOykSVsJ2",
              "uri": "spotify:artist:2ZUAe0H2nhsuuCOykSVsJ2"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273e906079c147bd06d1788ec90",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02e906079c147bd06d1788ec90",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851e906079c147bd06d1788ec90",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 6,
          "type": "album",
          "release_date": "1964-06-01",
          "href": "https://api.spotify.com/v1/albums/4bBbkwePNn99UrJrSNIs1L",
          "uri": "spotify:album:4bBbkwePNn99UrJrSNIs1L"
        },
        {
          "id": "1Sx2bn1LkVqGxb0ta44GI6",
          "name": "Miles and Monk at Newport (Mono Version) [Live]",
          "artists": [
            {
              "id": "1i4ed4I1y7YerI0fQV4lVc",
              "name": "Miles Davis Sextet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc",
              "uri": "spotify:artist:1i4ed4I1y7YerI0fQV4lVc"
            },
            {
              "id": "2ZUAe0H2nhsuuCOykSVsJ2",
              "name": "Thelonious Monk Quartet",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/2ZUAe0H2nhsuuCOykSVsJ2",
              "uri": "spotify:artist:2ZUAe0H2nhsuuCOykSVsJ2"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b2734c151cb32f17324af00f0523",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e024c151cb32f17324af00f0523",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d000048514c151cb32f17324af00f0523",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 6,
          "type": "album",
          "release_date": "1964-01-01",
          "href": "https://api.spotify.com/v1/albums/1Sx2bn1LkVqGxb0ta44GI6",
          "uri": "spotify:album:1Sx2bn1LkVqGxb0ta44GI6"
        },
        {
          "id": "2Ehj7dKoj92FtKQcz0NyTn",
          "name": "ブルーノート80年のヒット曲",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273d40057c5d6bd2c8ace89887d",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02d40057c5d6bd2c8ace89887d",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851d40057c5d6bd2c8ace89887d",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 54,
          "type": "album",
          "release_date": "2020-06-19",
          "href": "https://api.spotify.com/v1/albums/2Ehj7dKoj92FtKQcz0NyTn",
          "uri": "spotify:album:2Ehj7dKoj92FtKQcz0NyTn"
        },
        {
          "id": "41QdLsWPC9hsRIo3eQoG73",
          "name": "The Finest",
          "artists": [
            {
              "id": "0kbYTNQb4Pb1rPbbaF0pT4",
              "name": "Miles Davis",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0kbYTNQb4Pb1rPbbaF0pT4",
              "uri": "spotify:artist:0kbYTNQb4Pb1rPbbaF0pT4"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27374ad3398959324811d2dac18",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0274ad3398959324811d2dac18",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485174ad3398959324811d2dac18",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 18,
          "type": "album",
          "release_date": "2020-05-29",
          "href": "https://api.spotify.com/v1/albums/41QdLsWPC9hsRIo3eQoG73",
          "uri": "spotify:album:41QdLsWPC9hsRIo3eQoG73"
        },
        {
          "id": "4e9imW2ZWpqfvBbKGSXsWL",
          "name": "Blue Note Records: Beyond the Notes",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273f66dad9dbf9d0013e40d885e",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02f66dad9dbf9d0013e40d885e",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851f66dad9dbf9d0013e40d885e",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 34,
          "type": "album",
          "release_date": "2019-08-30",
          "href": "https://api.spotify.com/v1/albums/4e9imW2ZWpqfvBbKGSXsWL",
          "uri": "spotify:album:4e9imW2ZWpqfvBbKGSXsWL"
        },
        {
          "id": "45dzdwCRLo2Z3FYO03jEvC",
          "name": "Blue Note Records: Beyond The Notes (Original Motion Picture Soundtrack)",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b2738845843bde4ae404f2d1fa5f",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e028845843bde4ae404f2d1fa5f",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d000048518845843bde4ae404f2d1fa5f",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 21,
          "type": "album",
          "release_date": "2019-08-14",
          "href": "https://api.spotify.com/v1/albums/45dzdwCRLo2Z3FYO03jEvC",
          "uri": "spotify:album:45dzdwCRLo2Z3FYO03jEvC"
        },
        {
          "id": "7whdu2NmR5JMj93FGX5dRk",
          "name": "Fridays Of Jazz",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b2735b90d1cd63059b6be07ef4f8",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e025b90d1cd63059b6be07ef4f8",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d000048515b90d1cd63059b6be07ef4f8",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 9,
          "type": "album",
          "release_date": "2017-02-17",
          "href": "https://api.spotify.com/v1/albums/7whdu2NmR5JMj93FGX5dRk",
          "uri": "spotify:album:7whdu2NmR5JMj93FGX5dRk"
        },
        {
          "id": "5sDTyhAJQb5vdjZbgn0DEy",
          "name": "Thursday's Jazz",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273007e007b6ea2da70f34b0659",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02007e007b6ea2da70f34b0659",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851007e007b6ea2da70f34b0659",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 9,
          "type": "album",
          "release_date": "2017-02-16",
          "href": "https://api.spotify.com/v1/albums/5sDTyhAJQb5vdjZbgn0DEy",
          "uri": "spotify:album:5sDTyhAJQb5vdjZbgn0DEy"
        },
        {
          "id": "19H1zUehTGWnrjmilvhGQ8",
          "name": "Dancing Here On Your Own",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273a51d87bc06cbe5e2ad623fc1",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02a51d87bc06cbe5e2ad623fc1",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851a51d87bc06cbe5e2ad623fc1",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 9,
          "type": "album",
          "release_date": "2016-07-27",
          "href": "https://api.spotify.com/v1/albums/19H1zUehTGWnrjmilvhGQ8",
          "uri": "spotify:album:19H1zUehTGWnrjmilvhGQ8"
        },
        {
          "id": "7j8zzg392CqbbgrHHRlVZ3",
          "name": "The Complete Prestige 10-Inch LP Collection",
          "artists": [
            {
              "id": "0kbYTNQb4Pb1rPbbaF0pT4",
              "name": "Miles Davis",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0kbYTNQb4Pb1rPbbaF0pT4",
              "uri": "spotify:artist:0kbYTNQb4Pb1rPbbaF0pT4"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273f49f19a393e5c931a5b83ec2",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02f49f19a393e5c931a5b83ec2",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851f49f19a393e5c931a5b83ec2",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 46,
          "type": "album",
          "release_date": "2016-06-03",
          "href": "https://api.spotify.com/v1/albums/7j8zzg392CqbbgrHHRlVZ3",
          "uri": "spotify:album:7j8zzg392CqbbgrHHRlVZ3"
        },
        {
          "id": "078a06Xx8VriEh0l8B6TlC",
          "name": "Nightclub, Vol. 64 (The Golden Era of Bebop Music)",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b273387450e52220b6f662e4c591",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e02387450e52220b6f662e4c591",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00004851387450e52220b6f662e4c591",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 22,
          "type": "album",
          "release_date": "2016-04-08",
          "href": "https://api.spotify.com/v1/albums/078a06Xx8VriEh0l8B6TlC",
          "uri": "spotify:album:078a06Xx8VriEh0l8B6TlC"
        },
        {
          "id": "0U3QwwfOWsIXCJyBb5iU7o",
          "name": "Rough Boplicity",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b2735c19142b91b2c3e214629023",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e025c19142b91b2c3e214629023",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d000048515c19142b91b2c3e214629023",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 22,
          "type": "album",
          "release_date": "2015-11-09",
          "href": "https://api.spotify.com/v1/albums/0U3QwwfOWsIXCJyBb5iU7o",
          "uri": "spotify:album:0U3QwwfOWsIXCJyBb5iU7o"
        },
        {
          "id": "4Gm5JdkgoE70BLnotm2KcX",
          "name": "30 Soulful Moments in Jazz",
          "artists": [
            {
              "id": "0LyfQWJT6nXafLPZqxe9Of",
              "name": "Various Artists",
              "images": null,
              "genres": null,
              "href": "https://api.spotify.com/v1/artists/0LyfQWJT6nXafLPZqxe9Of",
              "uri": "spotify:artist:0LyfQWJT6nXafLPZqxe9Of"
            }
          ],
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67616d0000b27386e4b7c69847da2a1c593741",
              "width": 640,
              "height": 640
            },
            {
              "url": "https://i.scdn.co/image/ab67616d00001e0286e4b7c69847da2a1c593741",
              "width": 300,
              "height": 300
            },
            {
              "url": "https://i.scdn.co/image/ab67616d0000485186e4b7c69847da2a1c593741",
              "width": 64,
              "height": 64
            }
          ],
          "total_tracks": 30,
          "type": "album",
          "release_date": "2014-11-10",
          "href": "https://api.spotify.com/v1/albums/4Gm5JdkgoE70BLnotm2KcX",
          "uri": "spotify:album:4Gm5JdkgoE70BLnotm2KcX"
        }
      ],
      "limit": 20,
      "offset": 0,
      "total": 50,
      "previous": null,
      "next": "https://api.spotify.com/v1/artists/1i4ed4I1y7YerI0fQV4lVc/albums?offset=20&limit=20&include_groups=album,single,compilation,appears_on"
    }
};
