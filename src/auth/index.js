import querystring from 'querystring';
import { cookies, localStorage } from '../utils';

// authenticate user by passing data to cookie and localeStorage during login
export const authenticate = (response, callback) => {
  console.log('AUTHENTICATE HELPER ON LOGIN RESPONSE', response);
  cookies.set('token', response.data.token, response.data.expires);
  localStorage.set('user', response.data.user);
  callback();
};

// authenticate user by passing data to cookie and localeStorage during login
export const authenticateWithURI = (windowLocation, callback) => {
  if (!windowLocation) return
  const tokens = querystring.parse(windowLocation.search.substring(1));

  console.log('AUTHENTICATE HELPER ON LOGIN RESPONSE', tokens);
  cookies.set('token', tokens, tokens.expires);
  localStorage.set('user', tokens.user);
  callback();
};

// accessing user infos from localStorage
export const isAuth = () => {
  if (typeof window !== 'undefined') {
    const tokens = cookies.get('token');
    if (tokens) {
      const userInfo = localStorage.get('user');
      if(userInfo) {
        return {
          tokens,
          userInfo
        };
      }
      return
    }
  }
};
