import { useState, useEffect, useCallback } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import { QUERY_ALBUM_BY_ID } from '../graphql';

import { isObjEmpty } from '../utils';

export const useAlbumItemFetch = albumId => {
  const [state, setState] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const client = useApolloClient();

  const fetchData = useCallback(async () => {
    setError(false);
    setLoading(true);

    try {
      const result = await client.query({
        query: QUERY_ALBUM_BY_ID,
        variables: {
          id: albumId
        }
      });

      setState(prev => ({
        ...prev,
        ...result.data.getAlbumById,
      }));

    } catch (error) {
      setError(true);
      console.log(error);
    }
    setLoading(false);
  }, [albumId, client])

  // search for album by ID
  // loading cache if already present
  useEffect(() => {
    let cache = localStorage[`AlbumItem-${albumId}`];
    if (cache && !isObjEmpty(cache)) {
      cache = JSON.parse(cache)
      setState(cache);
      setLoading(false);
    } else {
      fetchData();
    }
  }, [fetchData, albumId]);

  // set albumId in cache when state changes
  useEffect(() => {
    if (!isObjEmpty(state)) localStorage.setItem([`AlbumItem-${albumId}`], JSON.stringify(state));
  }, [albumId, state])

  return [{ state, loading, error }, fetchData];
}
