import { useState, useEffect, useCallback } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import { QUERY_ALBUMS_BY_ARTIST } from '../graphql';

import { isObjEmpty } from '../utils';

export const useAlbumFetch = artistId => {
  const [state, setState] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const client = useApolloClient();

  const fetchData = useCallback(async () => {
    setError(false);
    setLoading(true);

    try {
      const result = await client.query({
        query: QUERY_ALBUMS_BY_ARTIST,
        variables: {
          artistId: artistId
        }
      });


      setState(prev => ({
        ...prev,
        ...result.data.getAlbumsByArtistId,
      }));

    } catch (error) {
      setError(true);
      console.log(error);
    }
    setLoading(false);
  }, [artistId, client])

  // search for album by ID
  // loading cache if already present
  useEffect(() => {
    let cache = localStorage[`AlbumByArtist-${artistId}`];
    if (cache && !isObjEmpty(cache)) {
      cache = JSON.parse(cache);
      setState(cache);
      setLoading(false);
    } else {
      fetchData();
    }
  }, [fetchData, artistId]);

  // set artistId in cache when state changes
  useEffect(() => {
    if (!isObjEmpty(state)) localStorage.setItem([`AlbumByArtist-${artistId}`], JSON.stringify(state));
  }, [artistId, state])

  return [{ state, loading, error }, fetchData];
}
