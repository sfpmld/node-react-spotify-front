import { useState, useEffect, useCallback } from 'react';
import { QUERY_ARTIST_BY_ID } from '../graphql'
import { useApolloClient } from '@apollo/react-hooks';

import { isObjEmpty } from '../utils';

export const useArtistFetch = artistId => {
  const [state, setState] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const client = useApolloClient();

  const fetchData = useCallback(async () => {
    setError(false);
    setLoading(true);

    try {
      const result = await client.query({
        query: QUERY_ARTIST_BY_ID,
        variables: {
          id: artistId
        }
      });

      // refresh state with new artist infos
      setState({
        ...result.data.getArtistById
      })

    } catch (error) {
      setError(true);
    }
    setLoading(false);
  }, [artistId, client])

  // search for artist by ID
  // loading cache if already present
  useEffect(() => {
    if (localStorage[artistId]) {
      setState(JSON.parse(localStorage[artistId]));
      setLoading(false);
    } else {
      fetchData();
    }
  }, [fetchData, artistId]);

  // set artistId in cache when state changes
  useEffect(() => {
    if (!isObjEmpty(state)) sessionStorage.setItem(artistId, JSON.stringify(state));
  }, [artistId, state])

  return [{ state, loading, error }, fetchData];
}
