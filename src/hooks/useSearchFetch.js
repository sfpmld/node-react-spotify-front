import { useState, useEffect, useCallback } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import { QUERY_SEARCH_ARTIST, QUERY_MORE_ARTIST } from '../graphql';

export const useSearchFetch = searchTerm => {
  const [state, setState] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const client = useApolloClient();

  const fetchData = useCallback(
    async (scope = 'init') => {
      setError(false);
      setLoading(true);

      try {
        let result;
        if (scope === 'init') {
          if (sessionStorage[searchTerm]) {
            setState(JSON.parse(sessionStorage[searchTerm]));
            setLoading(false);
            return;
          } else {
            result = await client.query({
              query: QUERY_SEARCH_ARTIST,
              variables: {
                data: {
                  q: searchTerm,
                  type: 'artist',
                },
              },
            });
          }
          // Load more
        } else {
          result = await client.query({
            query: QUERY_MORE_ARTIST,
            variables: {
              url: state.next,
            },
          });
        }

        // new state
        const data =
          scope === 'init' ? result.data.search : result.data.searchMore;

        // refresh state with new artist infos
        setState(prev => {
          const newState = {
            ...prev,
            previous: data.previous,
            next: data.next,
            artists:
              scope === 'next'
                ? [...prev.artists, ...data.artists]
                : [...data.artists],
            offset: data.offet,
            limit: data.limit,
            total: data.total,
          };

          sessionStorage.setItem(searchTerm, JSON.stringify(newState));

          return newState;
        });
      } catch (error) {
        console.log(error);
        setError(error);
      }

      setLoading(false);
    },
    [searchTerm, client, state.next]
  );

  // search for artist by ID
  // loading cache if already present
  useEffect(() => {
    if (!searchTerm) return;
    if (!sessionStorage[searchTerm]) {
      fetchData('init');
    }
  }, [fetchData, searchTerm]);

  return [{ state, loading, error }, fetchData];
};
