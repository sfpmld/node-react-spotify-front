// Configuration for spotify proxy api
export const API_URL = process.env.REACT_APP_API_URL;

export const APP_PORT = process.env.REACT_APP_PORT;

export const BACKDROP_URL = "https://image.freepik.com/free-photo/3d-rendering-perspective-cement-concrete-backdrop-plaster-stucco-texture-surface_163855-5.jpg"
