import { Fetcher } from '../utils';

export class RestClient {

  constructor(){
    this.fetcher = new Fetcher();
  }

  get(path, settings) {
    return this.fetcher.get(path, settings);
  }
  post(path, settings) {
    return this.fetcher.post(path, settings);
  }
}
