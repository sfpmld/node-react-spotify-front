import React from 'react';
import { useQuery }  from '@apollo/react-hooks';
import { Redirect } from 'react-router-dom'
import { GET_CURRENT_USER } from '../graphql/queries';


const withAuth = conditionFn => Component => props => {

    const { loading, error, data } = useQuery(GET_CURRENT_USER);

    if(loading) return <div>loading</div>
    console.log(error);
    if(error) return <div>Error</div>

    // Conditional check : if false redirecting to home page
    return conditionFn(data) ? <Component {...props} /> : <Redirect to="/" />
}

export default withAuth;
