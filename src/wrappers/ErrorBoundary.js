import React from 'react';
import useErrorBoundary from 'use-error-boundary';

const RenderErrorView = ({ error }) => {
  return (
    <>
      <div>{error.error.message}</div>
    </>
  );
};

const ErrorBoundary = ({ children }) => {
  const {
    // The Wrapper Class
    ErrorBoundary,
    // Error | null
  } = useErrorBoundary();

  return (
    <ErrorBoundary
      render={() => children}
      renderError={error => <RenderErrorView error={error} />}
    />
  );
};

export default ErrorBoundary;
