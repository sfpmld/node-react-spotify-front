import { gql } from 'apollo-boost';

export const artistItemFragments = {
  completeArtistItem: gql`
    fragment CompleteArtistItem on ArtistItem {
      id
      name
      images {
        url
        width
        height
      }
      genres
      href
      uri
    }
  `
};

export const artistFragments = {
  pagination: gql`
    fragment PaginationArtist on Artist {
      previous
      next
      offset
      limit
      total
    }
  `
};

export const albumFragments = {
  pagination: gql`
    fragment PaginationAlbum on Album {
      previous
      next
      offset
      limit
      total
    }
  `
};

export const trackFragments = {
  pagination: gql`
    fragment PaginationTrack on Track {
      previous
      next
      offset
      limit
      total
    }
  `
}

  export const imageFragments = {
    properties: gql`
      fragment PropsImage on Image {
        url
        width
        height
      }
    `
  };
