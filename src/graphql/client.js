import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { setContext } from 'apollo-link-context';

import { cookies, localStorage } from '../utils';

import { API_URL } from '../config';
import { isAuth } from '../auth';

export const getClient = () => {
  const graphQLEnpoint = `${API_URL}/graphql`;

  // 1. create http link
  const httpLink = new HttpLink({
    uri: graphQLEnpoint,
  });

  // 2. setContext for authtoken
  const authLink = setContext((_, { headers }) => {
    const token = isAuth();
    return {
      headers: {
        ...headers,
        Authorization: `Bearer ${token.tokens.access_token}`,
      },
    };
  });

  // 3. error handling
  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.forEach(({ message, locations, path }) => {
        if (message === 'Request failed with status code 401') {
          localStorage.remove('user');
          cookies.remove('token');
        }
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        );
      });

    if (networkError) console.log(`[Network error]: ${networkError}`);
  });

  // 4. concat http and authtoken link
  const links = ApolloLink.from([authLink, errorLink, httpLink]);

  return new ApolloClient({
    cache: new InMemoryCache({
      addTypename: false
    }),
    link: links,
  });
};
