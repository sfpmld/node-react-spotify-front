import { gql } from 'apollo-boost';
import {
  artistFragments,
  artistItemFragments,
  albumFragments,
  imageFragments,
} from '../fragment';

export const QUERY_SEARCH_ARTIST = gql`
  query($data: searchInput) {
    search(data: $data) {
      ... on Artist {
        artists {
          id
          name
          images {
            ...PropsImage
          }
          genres
          href
          uri
        }
        ...PaginationArtist
      }
    }
  }
  ${imageFragments.properties}
  ${artistFragments.pagination}
`;

export const QUERY_MORE_ARTIST = gql`
  query($url: String!) {
    searchMore(url: $url) {
      ... on Artist {
        artists {
          id
          name
          images {
            ...PropsImage
          }
          genres
          href
          uri
        }
        ...PaginationArtist
      }
    }
  }
  ${imageFragments.properties}
  ${artistFragments.pagination}
`;

export const QUERY_ARTIST_BY_ID = gql`
  query($id: String!) {
    getArtistById(id: $id) {
      id
      name
      images {
        ...PropsImage
      }
      genres
      href
      uri
    }
  }
  ${imageFragments.properties}
`;

export const QUERY_GET_CURRENT_USER = gql`
  query {
    getProfile {
      id
      display_name
      email
      images
      product
      href
      uri
    }
  }
`;

export const QUERY_ALBUMS_BY_ARTIST = gql`
  query($artistId: String!) {
    getAlbumsByArtistId(artistId: $artistId) {
      albums {
        id
        name
        artists {
          ...CompleteArtistItem
        }
        images {
        ...PropsImage
        }
        total_tracks
        type
        release_date
        href
        uri
      }
      ...PaginationAlbum
    }
  }
  ${artistItemFragments.completeArtistItem}
  ${imageFragments.properties}
  ${albumFragments.pagination}
`;

export const QUERY_ALBUM_BY_ID = gql`
  query($id: String!) {
    getAlbumById(id: $id) {
      id
      name
      artists {
        ...CompleteArtistItem
      }
      images {
        ...PropsImage
      }
      total_tracks
      type
      release_date
      href
      uri
    }
  }
  ${artistItemFragments.completeArtistItem}
  ${imageFragments.properties}
`;

export const QUERY_TRACKS_BY_ALBUM = gql`
  query($albumId: String!) {
    getTracksByAlbum(albumId: $albumId) {
      tracks {
        id
        name
        track_number
        duration_ms
        artists {
          ...CompleteArtistItem
        }
        href
        uri
      }
    }
  }
  ${artistItemFragments.completeArtistItem}
`;
