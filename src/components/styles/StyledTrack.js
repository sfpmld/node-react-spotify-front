import styled from 'styled-components';

export const StyledTrack = styled.div`
  font-family: 'Abel', sans-serif;
  color: #fff;
  background: #1c1c1c;
  border-radius: 20px;
  padding: 5px;
  text-align: center;

  .track-name {
    display: block;
    font-size: 18px;
    margin: 10px 0 0 0;
  }

  .track-info {
    display: block;
    font-size: 16px;
    margin: 0 0 10px 0;
  }
`;
