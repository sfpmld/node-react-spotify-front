import React, { useState, useCallback } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import { getClient } from '../graphql';

import Layout from './Layout';
import LoginPage from './LoginPage';
import Home from './Home';
import Artist from './Artist';
import Album from './Album';
import AlbumItem from './AlbumItem';
import NotFound from './NotFound';
import { AuthContext } from './context/auth-context';

import { GlobalStyle } from './styles/GlobalStyle';
import { isAuth } from '../auth';

const client = getClient();

const App = () => {
  // let routes;
  // AuthContext initialization
  const [isLoggedIn, setIsLoggedIn] = useState(isAuth() ? true : false);
  const login = useCallback(() => {
    setIsLoggedIn(true);
  }, []);
  const logout = useCallback(() => {
    setIsLoggedIn(false);
  }, []);

  return (
    <>
      <AuthContext.Provider
        value={{
          isLoggedIn,
          login,
          logout,
        }}>
        <ApolloProvider client={client}>
          <BrowserRouter>
            <Switch>
              <Layout>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/artist/:artistId" component={Artist} />
                <Route exact path="/albums/:artistId" component={Album} />
                <Route exact path="/albumItem/:albumId" component={AlbumItem} />
                <Route exact path="/not-found" component={NotFound} />
              </Layout>
            </Switch>
          </BrowserRouter>
        </ApolloProvider>
      </AuthContext.Provider>
      <GlobalStyle />
    </>
  );
};

export default App;
