import React from 'react';

// Components
import AlbumItemInfo from './elements/AlbumItemInfo';
import AlbumItemInfoBar from './elements/AlbumItemInfoBar';
import Spinner from './elements/Spinner';

import { useAlbumItemFetch } from '../hooks';

const AlbumItem = ({ match }) => {
  const { albumId } = match.params;

  const [{ state, loading, error }] = useAlbumItemFetch(albumId);
  const album = state;

  if (error) return <div>Something went wrong ...{error}</div>;
  if (loading) return <Spinner />;

  return (
  <>
    <AlbumItemInfo album={album} />
    <AlbumItemInfoBar
      albumId={albumId}
    />
  </>
  )
};

export default AlbumItem;
