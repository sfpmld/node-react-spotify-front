import React from 'react';
import { filterImageSize } from '../utils';

// Components
import AlbumThumb from './elements/AlbumThumb';
import MyButton from './elements/MyButton';

import Grid from './elements/Grid';
import Spinner from './elements/Spinner';
import NoImage from './images/no_image_small.jpg'

import { useAlbumFetch } from '../hooks';


const Album = ({ match }) => {
  const { artistId } = match.params;
  const [{ state, loading, error }] = useAlbumFetch(artistId);
  const albums = state.albums;
  const next = state.next;

  const loadMoreAlbums = () => {
    console.log(state);
  }

  if (error) return <div>Something went wrong ...</div>;
  if (loading) return <Spinner />;

  return (
    <>
      <Grid header='Album list for this artist'>
        {albums && albums.map(album => (
          <AlbumThumb
            key={album.id}
            albumId={album.id}
            albumName={album.name}
            clickable
            image={album.images ? filterImageSize(album.images) : NoImage}
          />
        ))}
      </Grid>
      {loading && <Spinner />}
      {next && !loading && (
        <MyButton text="Load More" callback={loadMoreAlbums} />
      )}
  </>
  )
};

export default Album;
