import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/react-testing';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import * as auth from '../auth';

import Artist from './Artist';

import { QUERY_ARTIST_BY_ID } from '../graphql/query';
import { artistByIdData } from '../test/fixtures';

describe('Artist.js', () => {

  afterEach(() => cleanup())

  const mocks = [
    {
      request: {
        query: QUERY_ARTIST_BY_ID,
        variables: {
          id: '1i4ed4I1y7YerI0fQV4lVc',
        },
      },
      result: {
        data: {
          ...artistByIdData,
        },
      },
    },
  ];

  it('should render artists data', async () => {
    const history = createMemoryHistory();
    const spyIsAuth = jest.spyOn(auth, 'isAuth');
    spyIsAuth.mockReturnValueOnce(true);

    const { getByTestId } = render(
      <Router history={history}>
        <MockedProvider
          mocks={mocks}
          addTypename={false}
          defaultOptions={{
            watchQuery: {
              fetchPolicy: 'no-cache',
            },
            query: {
              fetchPolicy: 'no-cache',
            },
          }}>
          <Artist
            match={{
                params: {
                  artistId: '1i4ed4I1y7YerI0fQV4lVc',
                },
            }}
          />
        </MockedProvider>
      </Router>
    );

    const expectedName = artistByIdData.getArtistById.name;
    const artistHeader = await waitForElement(() =>
      getByTestId('artist-header')
    );

    expect(artistHeader.innerHTML).toEqual(expectedName);
  });
});
