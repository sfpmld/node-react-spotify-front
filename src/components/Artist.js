import React from 'react';
import { useHistory } from 'react-router-dom';

// Components
import ArtistInfo from './elements/ArtistInfo';
import ArtistInfoBar from './elements/ArtistInfoBar';
import Spinner from './elements/Spinner';
import MyButton from './elements/MyButton';

import { useArtistFetch } from '../hooks';

const Artist = ({ match }) => {
  const { artistId } = match.params;

  const history = useHistory();
  const [{ state, loading, error }] = useArtistFetch(artistId);
  const artist = state;

  const seeAlbums = () => {
    history.push(`/albums/${artistId}`);
  }

  if (error) return <div>Something went wrong ...{error}</div>;
  if (loading) return <Spinner />;

  return (
  <>
    <ArtistInfo artist={artist} />
    <ArtistInfoBar
      genres={artist.genres ? artist.genres.join(',') : 'N/A'}
    />
    <MyButton text="See his album list" callback={seeAlbums} />
  </>
  )
};

export default Artist;
