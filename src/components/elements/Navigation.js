import React, { useContext } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { AuthContext } from '../context/auth-context';

import { StyledNavigation } from '../styles/StyledNavigation';

const Navigation = ({ match, location }) => {

  const auth = useContext(AuthContext);

  const isActive = path => {
    return location.pathname.match(path) ? true : false;
  };

  return (
    <StyledNavigation>
      <div className="navigation-content">
        <Link to="/" style={isActive('/') ? { color: 'blue' } : { color: '#fff' }}>
          <p>Home</p>
        </Link>
        <p>|</p>
        {!auth.isLoggedIn && <Link to="/login" style={isActive('/login') ? { color: '#000' } : { color: '#fff' }}>
        <p>Login</p>
        </Link>}
        {isActive('/artist/[wd]*') && <Link to={location.pathname} style={isActive('/artist/[wd]*') ? { color: '#000' } : { color: '#fff' }}>
        <p>Artist Details</p>
        </Link>}
        {isActive('/albums/[wd]*') && <Link to={location.pathname} style={isActive('/albums/[wd]*') ? { color: '#000' } : { color: '#fff' }}>
        <p>Albums List</p>
        </Link>}
        {isActive('/albumItem/[wd]*') && <Link to={location.pathname} style={isActive('/albumItem/[wd]*') ? { color: '#000' } : { color: '#fff' }}>
        <p>Album Details</p>
        </Link>}
      </div>
    </StyledNavigation>
  )
};

export default withRouter(Navigation);
