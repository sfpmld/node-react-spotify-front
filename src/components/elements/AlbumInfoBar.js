import React from 'react';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';

import { StyledAlbumInfoBar } from '../styles/StyledAlbumInfoBar';

const AlbumInfoBar = ({ release_date }) => (
  <StyledAlbumInfoBar>
    <div className="artistinfobar-content">
      <div className="artistinfobar-content-col">
        <FontAwesome className="fa-time" name="release_date" size="2x" />
        <span className="artistinfobar-info">
          Release Date: {release_date}
        </span>
      </div>

    </div>
  </StyledAlbumInfoBar>
);

AlbumInfoBar.propTypes = {
  release_date: PropTypes.instanceOf(Date),
}

export default AlbumInfoBar;
