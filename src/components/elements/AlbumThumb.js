import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { StyledAlbumThumb } from '../styles/StyledAlbumThumb';

const AlbumThumb = ({ image, albumId, albumName, clickable }) => (
  <StyledAlbumThumb>
    <h3 data-testid={`album-header-${albumId}`}>{albumName}</h3>
    {clickable ? (
      <Link to={`/albumItem/${albumId}`}>
        <img className="clickable" src={image} alt="albumthumb" />
      </Link>
    ) : (
      <img src={image} alt="albumthumb" />
    )}
  </StyledAlbumThumb>
);

AlbumThumb.propTypes = {
  image: PropTypes.string,
  albumId: PropTypes.string,
  clickable: PropTypes.bool,
};

export default AlbumThumb;
