import React from 'react';
import { Link } from 'react-router-dom';

import {
    StyledHeader,
} from '../styles/StyledHeader';


const Header = () => (
<StyledHeader>
  <div className="header-content">
    <Link to="/">
      <div></div>
    </Link>
  </div>
</StyledHeader>
)

export default Header;
