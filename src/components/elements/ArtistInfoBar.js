import React from 'react';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';

import { StyledArtistInfoBar } from '../styles/StyledArtistInfoBar';

const ArtistInfoBar = ({ genres }) => (
  <StyledArtistInfoBar>
    <div className="artistinfobar-content">
      <div className="artistinfobar-content-col">
        <FontAwesome className="fa-time" name="release" size="2x" />
        <span className="artistinfobar-info">
          Genres: {genres}
        </span>
      </div>

    </div>
  </StyledArtistInfoBar>
);

ArtistInfoBar.propTypes = {
  release_date: PropTypes.instanceOf(Date),
}

export default ArtistInfoBar;
