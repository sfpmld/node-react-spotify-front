import React from 'react';
import PropTypes from 'prop-types';

import { StyledMyButton } from '../styles/StyledMyButton';

const MyButton = ({ text, callback }) => (
  <StyledMyButton data-testid="loadMore" type="button" onClick={callback}>
    {text}
  </StyledMyButton>
)

MyButton.propTypes = {
  text: PropTypes.string,
  callback: PropTypes.func,
}

export default MyButton;
