import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import { StyledArtistThumb } from "../styles/StyledArtistThumb";

const ArtistThumb = ({ image, artistId, artistName, clickable }) => (
  <StyledArtistThumb>
    <h3 data-testid={"grid-" + artistId}>{artistName}</h3>
    {clickable ? (
      <Link to={`/artist/${artistId}`}>
        <img className="clickable" src={image} alt="artistthumb" />
      </Link>
    ) : (
      <img src={image} alt="artistthumb" />
    )}
  </StyledArtistThumb>
);

ArtistThumb.propTypes = {
  image: PropTypes.string,
  artistId: PropTypes.string,
  clickable: PropTypes.bool,
};

export default ArtistThumb;
