import React from 'react';
import PropTypes from 'prop-types';
import { BACKDROP_URL } from '../../config';

import NoImage from '../images/no_image.jpg';

import AlbumThumb from './AlbumThumb';
import { StyledAlbumInfo } from '../styles/StyledAlbumInfo';

import { filterImageSize } from '../../utils';

const AlbumInfo = ({ album }) => (
  <StyledAlbumInfo backdrop={BACKDROP_URL}>
    <div className="albuminfo-content">
      <div className="albuminfo-thumb">
        <AlbumThumb
          image={
            album.images
              ? filterImageSize(album.images)
              : NoImage
          }
          clickable={false}
          alt="albumthumb"
        />
      </div>
      <div className="albuminfo-text">
        <h1 data-testid={`album-id-${album.id}`}>{album.name}</h1>
        <h3>GENRES</h3>
        <p>{album.genres}</p>
      </div>
    </div>
  </StyledAlbumInfo>
);

AlbumInfo.propTypes = {
  album: PropTypes.object,
  images: PropTypes.array,
  genres: PropTypes.array,
}

export default AlbumInfo;
