import React from 'react';
import PropTypes from 'prop-types';

import { StyledHeroImage } from '../styles/StyledHeroImage';
import { filterImageSize } from '../../utils';

const HeroImage = ({ images, name, genres }) => (
  <StyledHeroImage image={filterImageSize(images)}>
    <div className="heroimage-content">
      <div className="heroimage-genres">
        <h1>{name}</h1>
        <p>{genres}</p>
      </div>
    </div>
  </StyledHeroImage>
)

HeroImage.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  genres: PropTypes.string,
}

export default HeroImage;
