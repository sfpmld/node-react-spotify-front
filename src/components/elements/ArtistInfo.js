import React from 'react';
import PropTypes from 'prop-types';
import { BACKDROP_URL } from '../../config';
import { filterImageSize } from '../../utils';

import NoImage from '../images/no_image.jpg';

import ArtistThumb from './ArtistThumb';

import { StyledArtistInfo } from '../styles/StyledArtistInfo';

const ArtistInfo = ({ artist }) => (
  <StyledArtistInfo backdrop={BACKDROP_URL}>
    <div className="artistinfo-content">
      <div className="artistinfo-thumb">
        <ArtistThumb
          image={
            artist.images
              ? filterImageSize(artist.images)
              : NoImage
          }
          clickable={false}
          alt="artistthumb"
        />
      </div>
      <div className="artistinfo-text">
        <h1 data-testid="artist-header">{artist.name}</h1>
        <h3>GENRES</h3>
        <p>{artist.genres}</p>
      </div>
    </div>
  </StyledArtistInfo>
);

ArtistInfo.propTypes = {
  artist: PropTypes.object,
  images: PropTypes.array,
  genres: PropTypes.array,
}

export default ArtistInfo;
