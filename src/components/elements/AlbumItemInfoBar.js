import React from 'react';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';

import Spinner from '../elements/Spinner';
import { StyledAlbumInfoBar } from '../styles/StyledAlbumInfoBar';
import { useTrackFetch } from '../../hooks';

import { msecToTime } from '../../utils';

const AlbumInfoBar = ({ albumId }) => {
  const [{ state, loading, error }] = useTrackFetch(albumId);
  const tracks = state.tracks;


  if (error) return <div>Something went wrong ...{error}</div>;
  if (loading) return <Spinner />;

  return (
    <StyledAlbumInfoBar>
      <div className="albuminfobar-content">
        <div className="albuminfobar-content-col">
          <FontAwesome className="fa-time" name="release_date" size="2x" />
            <ul>
            {tracks.map(track => (
                <li key={track.id} className="albuminfobar-info"><span data-testid={`track-id-${track.id}`} >{`${track.track_number} | ${track.name} | ${msecToTime(track.duration_ms)}`}</span></li>
            ))}
            </ul>
        </div>
      </div>
    </StyledAlbumInfoBar>
  );
};

AlbumInfoBar.propTypes = {
  release_date: PropTypes.instanceOf(Date),
};

export default AlbumInfoBar;
