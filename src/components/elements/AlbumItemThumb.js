import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { StyledAlbumThumb } from '../styles/StyledAlbumThumb';

const AlbumItemThumb = ({ image, albumId, clickable }) => (
  <StyledAlbumThumb>
    {clickable ? (
      <Link to={`/albums/${albumId}`}>
        <img className="clickable" src={image} alt="albumthumb" />
      </Link>
    ) : (
      <img src={image} alt="albumthumb" />
    )}
  </StyledAlbumThumb>
)

AlbumItemThumb.propTypes = {
  image: PropTypes.string,
  albumId: PropTypes.string,
  clickable: PropTypes.bool,
}

export default AlbumItemThumb;

