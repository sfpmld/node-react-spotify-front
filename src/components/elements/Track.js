import React from 'react';
import PropTypes from 'prop-types';

import { StyledTrack } from '../styles/StyledTrack';

const Track = ({ track }) => (
  <StyledTrack>
    <div data-testid={`track-${track.id}`} className="track-name">{track.name} {track.track.track_number} {track.duration_ms}</div>
  </StyledTrack>
);

Track.propTypes = {
  track: PropTypes.object,
}

export default Track;
