import React from 'react';
import { withRouter } from 'react-router-dom';
import Header from './elements/Header';
import Navigation from './elements/Navigation';

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Navigation />
      <div className="container">{children}</div>
    </>
  )
}

export default withRouter(Layout);
