import React from 'react';
import { act } from 'react-dom/test-utils';
import { render, screen, waitForElement, fireEvent, cleanup } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/react-testing';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import * as auth from '../auth';

import Home from './Home';

import { QUERY_SEARCH_ARTIST, QUERY_MORE_ARTIST } from '../graphql/query';
import { artistsData, moreArtistsData } from '../test/fixtures';

describe('Home.js', () => {

  afterEach(() => cleanup())

  const mocks = [
    {
      request: {
        query: QUERY_SEARCH_ARTIST,
        variables: {
          data: {
            q: 'Miles Davis',
            type: 'artist',
          },
        },
      },
      result: {
        data: {
          ...artistsData
        },
      },
    },
    {
      request: {
        query: QUERY_MORE_ARTIST,
        variables: {
          url: 'https://api.spotify.com/v1/search?query=Miles+davis&type=artist&offset=2&limit=2',
        },
      },
      result: {
        data: {
          ...moreArtistsData
        },
      },
    },
  ];

  it('should render artists after having fill a search term', async () => {
    const history = createMemoryHistory();
    const spyIsAuth = jest.spyOn(auth, 'isAuth');
    spyIsAuth.mockReturnValueOnce(true);

    const { getByPlaceholderText, getByTestId } = render(
      <Router history={history}>
        <MockedProvider
          mocks={mocks}
          addTypename={false}
          defaultOptions={{
            watchQuery: {
              fetchPolicy: 'no-cache',
            },
            query: {
              fetchPolicy: 'no-cache',
            },
          }}>
          <Home />
        </MockedProvider>
      </Router>
    );

    const searchInput = getByPlaceholderText(/Search Artist/);
    // fill search input with artist name
    fireEvent.change(searchInput, { target: { value: 'Miles Davis' } });

    const expectedId = artistsData.search.artists[0].id;
    const expectedName = artistsData.search.artists[0].name;
    const artistsGrid = await waitForElement(() =>
      getByTestId('grid-' + expectedId)
    );

    expect(artistsGrid.innerHTML).toEqual(expectedName);
  });

  it('should render more artists after having clicked on load more button', async () => {
    const history = createMemoryHistory();
    const spyIsAuth = jest.spyOn(auth, 'isAuth');
    spyIsAuth.mockReturnValueOnce(true);

    const { getByPlaceholderText, getByTestId } = render(
      <Router history={history}>
        <MockedProvider
          mocks={mocks}
          addTypename={false}
          defaultOptions={{
            watchQuery: {
              fetchPolicy: 'no-cache',
            },
            query: {
              fetchPolicy: 'no-cache',
            },
          }}>
          <Home />
        </MockedProvider>
      </Router>
    );

    const searchInput = getByPlaceholderText(/Search Artist/);
    // fill search input with artist name
    fireEvent.change(searchInput, { target: { value: 'Miles Davis' } });
    let expectedId = artistsData.search.artists[0].id;
    let expectedName = artistsData.search.artists[0].name;
    let artistsGrid = await waitForElement(() =>
      getByTestId(`grid-${expectedId}`)
    );

    expect(artistsGrid.innerHTML).toEqual(expectedName);

    // // button load more
    const loadMoreButton = getByTestId('loadMore');

    act(() => {
      userEvent.click(loadMoreButton);
    });

    expectedId = moreArtistsData.searchMore.artists[0].id;
    expectedName = moreArtistsData.searchMore.artists[0].name;
    artistsGrid = await waitForElement(() => {
      return screen.getByTestId(`grid-${expectedId}`)
    });
    expect(artistsGrid.innerHTML).toEqual(expectedName);

  });
});
