import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';

// import Components
import SearchBar from './elements/SearchBar';
import Grid from './elements/Grid';
import ArtistThumb from './elements/ArtistThumb';
import MyButton from './elements/MyButton';
import Spinner from './elements/Spinner';
import ErrorBoundary from '../wrappers/ErrorBoundary';
import { AuthContext } from './context/auth-context';

// Custom Hook
import { useSearchFetch } from '../hooks';

import NoImage from './images/no_image.jpg';
import { authenticateWithURI, isAuth } from '../auth';
import { filterImageSize } from '../utils';

const Home = () => {
  const history = useHistory();
  const auth = useContext(AuthContext);
  const [searchTerm, setSearchTerm] = useState();
  const [
    {
      state: { artists, next },
      loading,
      error,
    },
    fetchArtists,
  ] = useSearchFetch(searchTerm);
  const [logged, setLogged] = useState(auth.isLoggedIn);

  // Retrieving access token and refresh token if coming back from backend
  useEffect(() => {
    if (typeof window !== 'undefined' && window.location.search) {
      authenticateWithURI(window.location, () => {
        auth.login();
        setLogged(true);
      });
    }
  }, [auth, history]);

  useEffect(() => {
    if (searchTerm) fetchArtists('init');
    // for componentWillUnmount_behavior
  }, [searchTerm, fetchArtists]);

  useEffect(() => {
    if (isAuth() || logged) {
      // if (logged) {
      history.push(`/`);
    } else {
      history.push(`/login`);
    }
    // for componentWillUnmount_behavior
  }, [history, logged]);

  // Handlers
  const searchArtists = search => {
    // adding next artists to state
    setSearchTerm(search);
    // if (searchTerm) fetchArtists('init');
  };

  const loadMoreArtists = () => {
    if (next) {
      fetchArtists('next');
    }
  };

  if (error) return <div>Something went wrong ...</div>;
  if (loading) return <Spinner />;
  if (!artists) return <SearchBar callback={searchArtists} />;
  return (
    <ErrorBoundary>
      <SearchBar callback={searchArtists} />
      <Grid header={searchTerm ? 'Search Result' : ''}>
        {artists.map(artist => (
          <ArtistThumb
            key={artist.id}
            clickable
            image={artist.images ? filterImageSize(artist.images) : NoImage}
            artistId={artist.id}
            artistName={artist.name}
          />
        ))}
      </Grid>
      {loading && <Spinner />}
      {next && !loading && (
        <MyButton text="Load More" callback={loadMoreArtists} />
      )}
    </ErrorBoundary>
  );
};

export default Home;
