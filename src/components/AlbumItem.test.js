import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/react-testing';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import * as auth from '../auth';

import AlbumItem from './AlbumItem';

import { QUERY_ALBUM_BY_ID, QUERY_TRACKS_BY_ALBUM } from '../graphql/query';
import { albumByIdData, tracksByAlbumData } from '../test/fixtures';
import { msecToTime } from '../utils';

describe('AlbumItem.js', () => {
  afterEach(() => cleanup());

  const mocks = [
    {
      request: {
        query: QUERY_ALBUM_BY_ID,
        variables: {
          id: '0P3DTd8ug4xnh6VCgjSjuF',
        },
      },
      result: {
        data: {
          ...albumByIdData,
        },
      },
    },
    {
      request: {
        query: QUERY_TRACKS_BY_ALBUM,
        variables: {
          albumId: '0P3DTd8ug4xnh6VCgjSjuF',
        },
      },
      result: {
        data: {
          ...tracksByAlbumData,
        },
      },
    },
  ];

  it('should render album list for a given artist', async () => {
    const history = createMemoryHistory();
    const spyIsAuth = jest.spyOn(auth, 'isAuth');
    spyIsAuth.mockReturnValueOnce(true);

    const { getByTestId } = render(
      <Router history={history}>
        <MockedProvider
          mocks={mocks}
          addTypename={false}
          defaultOptions={{
            watchQuery: {
              fetchPolicy: 'no-cache',
            },
            query: {
              fetchPolicy: 'no-cache',
            },
          }}>
          <AlbumItem
            match={{
              params: {
                albumId: '0P3DTd8ug4xnh6VCgjSjuF',
              },
            }}
          />
        </MockedProvider>
      </Router>
    );

    // Album
    const expectedId = albumByIdData.getAlbumById.id;
    const expectedAlbumName = albumByIdData.getAlbumById.name;
    const AlbumHeader = await waitForElement(() =>
      getByTestId(`album-id-${expectedId}`)
    );
    expect(AlbumHeader.innerHTML).toEqual(`${expectedAlbumName}`);

    // Track
    const expectedTrackNb =
      tracksByAlbumData.getTracksByAlbum.tracks[0].track_number;
    const expectedTrackId = tracksByAlbumData.getTracksByAlbum.tracks[0].id;
    const expectedTrackName = tracksByAlbumData.getTracksByAlbum.tracks[0].name;
    const expectedDuration = msecToTime(
      tracksByAlbumData.getTracksByAlbum.tracks[0].duration_ms
    );
    const TrackContainer = await waitForElement(() =>
      getByTestId(`track-id-${expectedTrackId}`)
    );

    expect(TrackContainer.innerHTML).toEqual(
      `${expectedTrackNb} | ${expectedTrackName} | ${expectedDuration}`
    );
  });
});
