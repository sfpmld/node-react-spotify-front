import React from 'react';
// import { Link, Redirect } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.min.css';
import { API_URL } from '../config';

import MyButton from './elements/MyButton';
// import { authenticate, isAuth } from '../auth';

const LoginPage = () => {

  const loginToSpotify = async () => {
    window.location = `${API_URL}/oauth/login`
  }

  return (
    <MyButton text="Login to Spotify" callback={loginToSpotify} />
  )
}

export default LoginPage;
