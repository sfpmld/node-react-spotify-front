import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { MockedProvider } from '@apollo/react-testing';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import * as auth from '../auth';

import Album from './Album';

import { QUERY_ALBUMS_BY_ARTIST } from '../graphql/query';
import { albumsByArtistData } from '../test/fixtures';

describe('Album.js', () => {

  afterEach(() => cleanup())

  const mocks = [
    {
      request: {
        query: QUERY_ALBUMS_BY_ARTIST,
        variables: {
          artistId: '1i4ed4I1y7YerI0fQV4lVc',
        },
      },
      result: {
        data: {
          ...albumsByArtistData,
        },
      },
    },
  ];

  it('should render album list for a given artist', async () => {
    const history = createMemoryHistory();
    const spyIsAuth = jest.spyOn(auth, 'isAuth');
    spyIsAuth.mockReturnValueOnce(true);

    const { getByTestId } = render(
      <Router history={history}>
        <MockedProvider
          mocks={mocks}
          addTypename={false}
          defaultOptions={{
            watchQuery: {
              fetchPolicy: 'no-cache',
            },
            query: {
              fetchPolicy: 'no-cache',
            },
          }}>
          <Album
            match={{
              params: {
                artistId: '1i4ed4I1y7YerI0fQV4lVc',
              },
            }}
          />
        </MockedProvider>
      </Router>
    );

    const expectedId = albumsByArtistData.getAlbumsByArtistId.albums[1].id;
    const expectedName = albumsByArtistData.getAlbumsByArtistId.albums[1].name;
    const artistHeader = await waitForElement(() =>
      getByTestId(`album-header-${expectedId}`)
    );

    expect(artistHeader.innerHTML).toEqual(expectedName);
  });
});
